package org.javatraining.sample.samplecms.model;

/**
 *
 * @author team
 */
public class Group {
    String groupId;
    String groupName;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
}
