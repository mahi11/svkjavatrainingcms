package org.javatraining.sample.samplecms.model;

/**
 *
 * @author team
 */
public class Saved {
    String savedId;
    String savedUserId;
    String savedPostId;
    String savedCommentId;

    public String getSavedId() {
        return savedId;
    }

    public void setSavedId(String savedId) {
        this.savedId = savedId;
    }

    public String getSavedUserId() {
        return savedUserId;
    }

    public void setSavedUserId(String savedUserId) {
        this.savedUserId = savedUserId;
    }

    public String getSavedPostId() {
        return savedPostId;
    }

    public void setSavedPostId(String savedPostId) {
        this.savedPostId = savedPostId;
    }

    public String getSavedCommentId() {
        return savedCommentId;
    }

    public void setSavedCommentId(String savedCommentId) {
        this.savedCommentId = savedCommentId;
    }
}
