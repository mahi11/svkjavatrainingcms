package org.javatraining.sample.samplecms.model;


/**
 *
 * @author team
 */

public class User {
    int id;
    String userid;
    
    private String authToken;
    
    int points;

    public User(){
        points = 0;
        
    }
    
    public User(String userid) {
        points = 0;
        userid = userid;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
    
 
}
